<?php
/**
 * Template for displaying the footer
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

	</div><!-- #bodywrap -->



    <!-- GOOGLE ANALYTICS -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-67035046-2', 'auto');
      ga('send', 'pageview');
    </script>
    <!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter38208795 = new Ya.Metrika({ id:38208795, clickmap:true, trackLinks:true, accurateTrackBounce:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/38208795" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
	<div id="footer" role="contentinfo">
		<div id="colophon">

<?php
	/*
	 * A sidebar in the footer? Yep. You can can customize
	 * your footer with four columns of widgets.
	 */
	get_sidebar( 'footer' );
?>

			<div id="site-info">
              <!--Контетная часть подвала -->
<?php if ( is_active_sidebar( 'footer-1' ) ) : ?>
<?php dynamic_sidebar( 'footer-1' ); ?>
<?php endif; ?>

<?php if ( is_active_sidebar( 'footer-2' ) ) : ?>
<?php dynamic_sidebar( 'footer-2' ); ?>
<?php endif; ?>

<?php if ( is_active_sidebar( 'footer-3' ) ) : ?>
<?php dynamic_sidebar( 'footer-3' ); ?>
<?php endif; ?>

<?php if ( is_active_sidebar( 'footer-4' ) ) : ?>
<?php dynamic_sidebar( 'footer-4' ); ?>
<?php endif; ?>

<?php if ( is_active_sidebar( 'footer-5' ) ) : ?>
<?php dynamic_sidebar( 'footer-5' ); ?>
<?php endif; ?>
    <!--Контетная часть подвала END -->


			</div><!-- #site-info -->

			<div id="site-generator">

			</div><!-- #site-generator -->

		</div><!-- #colophon -->
	</div><!-- #footer -->

</div><!-- #wrapper -->

<?php
	/*
	 * Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
</div> <!-- mmenu div -->
</body>
</html>
