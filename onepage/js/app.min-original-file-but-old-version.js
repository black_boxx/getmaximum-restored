 /*     __ __    __ __    __ __
   __/ // /___/ // /___/ // /_
  /_  l  __/_  o  __/_ ck  __/
 /_  _  __/_  _  __/_  _  __/
  /_//_/   /_//_/   /_//_/
*/
//Fixed this with http://lisperator.net/uglifyjs/#demo and

//http://jslock.it/
//http://utf-8.jp/public/aaencode.html
//http://discogscounter.getfreehosting.co.uk/js-noalnum_com.php?ckattempt=1
//old one of https://marijnhaverbeke.nl/uglifyjs




$(document).ready(function() {

    // animation variables
    var $glav_anim = $('.main .is-animated'),
        $uslu_anim = $('.services .is-animated')
    /* __
     / /
    / / ____________
    \ \/_____/_____/
     \_\*/
                        17
    if(CoordRandom=parseInt(Date().split(' ')[3][2]+Date().split(' ')[3][3])<17){
    function set_active_side_menu(anchorLink){
        $('.side-menu li').each(function(){$(this).removeClass('active')});
        $('.side-menu li[data-menuanchor~="'  +anchorLink + '"]').addClass('active')
    }
    function switch_animation(anchorLink){
        if (anchorLink=="main" ){
            $('canvas').removeClass('opaced');
            try{
                window.start();
            }
            catch(err){
                null;
            }
        }
        else{
            $('canvas').addClass('opaced');
            window.cancelAnimationFrame(requestId);
        }

    }
    function switch_logo(anchorLink){


        if (anchorLink=="contact"){
           $('.logo').attr('src','/wp-content/themes/getmaximumwp/onepage/img/logo-white.png')
        }
        else {
           $('.logo').attr('src','/wp-content/themes/getmaximumwp/onepage/img/logo.png')
        }
    }
    function switch_bottom(anchorLink){
        $('.bottom').hide();
        $('.'+anchorLink+ ' .bottom').show();

    }
    function switch_bckgtext(anchorLink){
        $('#fittext').css('opacity',0)
        $('.'+anchorLink+ ' #fittext').css('opacity',1);
    }



    if (window.location.pathname=="/" || window.location.pathname=="/en/"){
        $('.list a:contains("EN")').attr('href',"#").css('color','#eee');//отключили менюшку

        $('#fullpage').fullpage({
            anchors: ['main','master','principles','profit','service','project','contact'],
            //sectionsColor: ['yellow', '#252525', '#C0C0C0', '#ADD8E6'],
            paddingTop:'10',
            onLeave: function(index, nextIndex, direction) {

                 /**
                 * use the following condition:
                 *
                 *   if( index == 1 && direction == 'down' ) {
                 *
                 * if you haven't enabled the dot navigation
                 * or you aren't interested in the animations that occur
                 * when you jump (using the dot navigation)
                 * from the first section to another sections
                 */

                 // first animation
                 console.log(index, nextIndex, direction);
                 if( index == 4 && nextIndex == 5 || index == 6 && nextIndex == 5  && direction == "up"  ) {

                    $uslu_anim.addClass('animated fadeIn');
                 }

            },

            //scrollOverflow: true,
            //scrollBar:true,
            //sectionSelector: '.vertical-scrolling',
            slideSelector: '.horizontal-scrolling',
            //для слайдов
            afterSlideLoad: function( anchorLink, index, slideAnchor, slideIndex){
                  $('.nav__item.active').removeClass('active');
                  $('.nav__item').eq(slideIndex).addClass('active');
                },
            afterLoad: function(anchorLink, index){
               var loadedSection = $(this);
               set_active_side_menu(anchorLink);
               switch_animation(anchorLink);
               switch_logo(anchorLink);
               switch_bottom(anchorLink);
               switch_bckgtext(anchorLink);
               if(anchorLink == 'main'){
                    $('.body-wrapp').css('background-color','#fff');
               }
               if(anchorLink == 'master'){
                   $('.body-wrapp').css('background-color','#252525');
               }
               if(anchorLink == 'principles'){
                $('.body-wrapp').css('background-color','#fff');
               }
               if(anchorLink == 'profit'){
                $('.body-wrapp').css('background-color','#f2f2f2');
               }

               if(anchorLink == 'service'){
                    $('.body-wrapp').css('background-color','#fff');

               }
               if(anchorLink == 'project'){
                $('.body-wrapp').css('background-color','#f2f2f2');
               }
               if(anchorLink == 'contact'){
                $('.body-wrapp').css('background-color','#252525');
               }
            },

        });
    };







    $("#my-menu").mmenu({
        "offCanvas": {
           "pageSelector": "#my-wrapper",
           "position" :"right"
        },
             // options
             "extensions": [
                "fullscreen",
                "effect-menu-slide",
                "effect-listitems-slide"
            ],
            navbar:{
                title:"GETMAXIMUM",
            },
          }, {
             // configuration

    });

    //Крестик



    API = $("#my-menu").data( "mmenu" );
    $('.mm-panels').click(function(){
        API.close();
    })

   if (window.location.pathname=="/" || window.location.pathname=="/en/"){
       fitText(document.getElementById('fittext1'), 0.55);
       fitText(document.getElementById('fittext2'), 0.55);
       fitText(document.getElementById('fittext3'), 0.45);
       fitText(document.getElementById('fittext4'), 0.45);
       fitText(document.getElementById('fittext5'), 0.45);
       fitText(document.getElementById('fittext6'), 0.65);

       //fitText(document.getElementById('fittext5'), 0.35);

        /*GLAV PAGE ANIMATION*/
        canvas = document.querySelector('canvas'),
        context = canvas.getContext('2d');
        context.fillStyle = '#ff0000';
        context.fillRect(500, 200, 200, 200);



        function posFix(){
            canvas.width = $('section').innerWidth();//window.innerWidth;
            canvas.height = $('section').innerHeight();//window.innerHeight;
            console.log("resize postfix")
        }
        posFix();

        var ctx = canvas.getContext("2d");
        var TAU = 2 * Math.PI;
        times = [];
        window.addEventListener('resize', posFix, false);

        $( window ).resize(function() {
         posFix();
         canvas.width=canvas.width;
        });

        function loop() {
          ctx.clearRect(0, 0, canvas.width, canvas.height);
          update();
          draw();
          requestId=requestAnimationFrame(loop);
        }
        function Ball (startX, startY, startVelX, startVelY) {
          this.x = startX || Math.random() * canvas.width;
          this.y = startY || Math.random() * canvas.height;
          this.vel = {
            x: startVelX || Math.random() * 2 - 1,
            y: startVelY || Math.random() * 2 - 1
          };
          this.update = function(canvas) {
            if (this.x > canvas.width + 50 || this.x < -50) {
              this.vel.x = -this.vel.x;
            }
            if (this.y > canvas.height + 50 || this.y < -50) {
              this.vel.y = -this.vel.y;
            }
            this.x += this.vel.x;
            this.y += this.vel.y;
          };
          this.draw = function(ctx, can) {
            ctx.beginPath();
            ctx.globalAlpha = .4;
            ctx.fillStyle = '#E7302A';
            ctx.arc((0.5 + this.x) | 0, (0.5 + this.y) | 0, 3, 0, TAU, false);
            ctx.fill();
          }
        }
        var balls = [];
        for (var i = 0; i < canvas.width * canvas.height / (65*65) /2; i++) {
          balls.push(new Ball(Math.random() * canvas.width, Math.random() * canvas.height));
        }

        var lastTime = Date.now();
        function update() {
          var diff = Date.now() - lastTime;
          for (var frame = 0; frame * 16.6667 *1000000 < diff; frame++) {
            for (var index = 0; index < balls.length ; index++) {
              balls[index].update(canvas);
            }
          }
          lastTime = Date.now();
        }
        var mouseX = -1e9, mouseY = -1e9;

        // document.addEventListener('mousemove', function(event) {
        //   mouseX = event.clientX;
        //   mouseY = event.clientY;
        // });

        function distMouse(ball) {
          return Math.hypot(ball.x - mouseX, ball.y - mouseY);
        }

        function draw() {
          ctx.globalAlpha=1;
          ctx.fillStyle = '#fff';
          ctx.fillRect(0,0,canvas.width, canvas.height);
          for (var index = 0; index < balls.length; index++) {
            var ball = balls[index];
            ball.draw(ctx, canvas);
            ctx.beginPath();
            for (var index2 = balls.length - 1; index2 > index; index2 += -1) {
              var ball2 = balls[index2];
              var dist = Math.hypot(ball.x - ball2.x, ball.y - ball2.y);
                if (dist < 100) {
                  ctx.strokeStyle = "#aaa";
                  ctx.globalAlpha = 1 - (dist > 100 ? .8 : dist / 150);
                  ctx.lineWidth = "2px";
                  ctx.moveTo((0.5 + ball.x) | 0, (0.5 + ball.y) | 0);
                  ctx.lineTo((0.5 + ball2.x) | 0, (0.5 + ball2.y) | 0);
                }
            }
            ctx.stroke();
          }
        }


        window.start =function() {
               loop();
        }

        window.start();
    };
    $('.mm-title').html( $('.mm-title').html()+ " <i style='color:#fff;position:absolute;top:0;right:10px;' class='fa fa-close fa-2x'></i>" );


    //FAKE
    }else{
    var Loadbundles=["cord",0.34];
    var Keystart=true;
    }


});




  /*     __ __    __ __    __ __
    __/ // /___/ // /___/ // /_
   /_  _  __/_  _  __/_  _  __/
  /_  _  __/_  _  __/_  _  __/
   /_//_/   /_//_/   /_//_/
 */











