<?php
/**
 * Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title> </title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<style>
    body > div > #my-menu{
        display: none!important;
    }
</style>

<meta charset="UTF-8">
<title>GETMAXIMUM</title>
<meta name="description" content="GETMAXIMUM">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="HandheldFriendly" content="true">
<script src="/wp-content/themes/getmaximumwp/onepage/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/wp-content/themes/getmaximumwp/onepage/bower_components/fullpage/jquery.fullPage.min.js"></script>
<script src="/wp-content/themes/getmaximumwp/onepage/bower_components/mmenu/dist/js/jquery.mmenu.all.min.js"></script>
<script src="/wp-content/themes/getmaximumwp/onepage/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>
<script src="/wp-content/themes/getmaximumwp/onepage/bower_components/jquery-parallax/scripts/jquery.parallax-1.1.3.js"></script>
<script src="/wp-content/themes/getmaximumwp/onepage/bower_components/jquery-parallax/scripts/jquery.localscroll-1.2.7-min.js"></script>
<!--  <script src="bower_components/jquery-parallax/scripts/jquery.scrollTo-1.4.2-min.js"></script> -->
<script src="/wp-content/themes/getmaximumwp/onepage/bower_components/fittext/fittext.js"></script>
<script src="/wp-content/themes/getmaximumwp/onepage/js/lib/arbor.js"></script>
<script src="/wp-content/themes/getmaximumwp/onepage/js/lib/arbor-tween.js"></script>
<script src="/wp-content/themes/getmaximumwp/onepage/js/lib/arbor-graphics.js"></script>


<!--
<link rel="stylesheet" href="bower_components/jquery-parallax/style.css"> -->
<link rel="stylesheet" href="/wp-content/themes/getmaximumwp/onepage/bower_components/fullpage/dist/jquery.fullpage.min.css">
<link rel="stylesheet" href="/wp-content/themes/getmaximumwp/onepage/bower_components/animate.css/animate.min.css">
<link rel="stylesheet" href="/wp-content/themes/getmaximumwp/onepage/bower_components/fontawesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/wp-content/themes/getmaximumwp/onepage/bower_components/mmenu/dist/css/jquery.mmenu.all.css">
<link href="/wp-content/themes/getmaximumwp/onepage/bower_components/mmenu/dist/extensions/effects/jquery.mmenu.effects.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="/wp-content/themes/getmaximumwp/onepage/bower_components/mmenu/dist/extensions/fullscreen/jquery.mmenu.fullscreen.css">
<link rel="stylesheet" href="/wp-content/themes/getmaximumwp/onepage/bower_components/owl.carousel/dist/assets/owl.carousel.min.css">
<link rel="stylesheet" href="/wp-content/themes/getmaximumwp/onepage/css/style.css">
<link rel="stylesheet" href="/wp-content/themes/getmaximumwp/onepage/css/style-wp.css">
<!--FAVICON-->
<link rel="apple-touch-icon" sizes="57x57" href="/wp-content/themes/getmaximumwp/onepage//img/fav/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/wp-content/themes/getmaximumwp/onepage//img/fav/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/wp-content/themes/getmaximumwp/onepage//img/fav/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/wp-content/themes/getmaximumwp/onepage//img/fav/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/wp-content/themes/getmaximumwp/onepage//img/fav/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/wp-content/themes/getmaximumwp/onepage//img/fav/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/wp-content/themes/getmaximumwp/onepage//img/fav/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/wp-content/themes/getmaximumwp/onepage//img/fav/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/wp-content/themes/getmaximumwp/onepage//img/fav/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/wp-content/themes/getmaximumwp/onepage//img/fav/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/wp-content/themes/getmaximumwp/onepage//img/fav/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/wp-content/themes/getmaximumwp/onepage//img/fav/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/wp-content/themes/getmaximumwp/onepage//img/fav/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/wp-content/themes/getmaximumwp/onepage//img/fav/manifest.json">
<link rel="mask-icon" href="/wp-content/themes/getmaximumwp/onepage//img/fav/safari-pinned-tab.svg" color="#5bbad5">
<link rel="shortcut icon" href="/wp-content/themes/getmaximumwp/onepage//img/fav/favicon.ico">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-TileImage" content="/wp-content/themes/getmaximumwp/onepage//img/fav/mstile-144x144.png">
<meta name="msapplication-config" content="/wp-content/themes/getmaximumwp/onepage//img/fav/browserconfig.xml">
<meta name="theme-color" content="#ffffff">
<?php
	/*
	 * We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/*
	 * Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>
</head>

<body <?php body_class(); ?>>

<!-- <nav id="my-menu">
   <ul>
      <li data-menuanchor="main"><a href="/#main">Компания</a></li>
      <li data-menuanchor="profit"><a href="/#profit">Выгоды</a></li>
      <li data-menuanchor="service"><a href="/#service">Услуги</a></li>
      <li data-menuanchor="project"><a href="/#project">Проекты</a></li>
      <li ><a href="/blog">Блог</a></li>
      <li ><a href="/wifi">WiFi</a></li>
      <li data-menuanchor="contact"><a href="/#contact">Контакты</a></li>

   </ul>
</nav> -->
<div>  <!-- this is mmenu div -->
<div id="my-menu"><?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?></div>


<div class="body-wrapp">

    <div class="header">
        <a href="/">
            <img class="logo" src="/wp-content/themes/getmaximumwp/onepage/img/logo.png" alt="GETMAXIMUM">
        </a>
        <a class="menu-button" href="#my-menu">
            <i class="fa fa-bars fa-3x"></i>
        </a>
    </div>
    <ul id="myMenu" class="side-menu">
        <a href="/#main"><li data-menuanchor="main" ></li></a>
        <a href="/#master"><li data-menuanchor="master"></li></a>
        <a href="/#principles"><li data-menuanchor="principles"></li></a>
        <a href="/#profit"><li data-menuanchor="profit"></li></a>
        <a href="/#service"><li data-menuanchor="service"></li></a>
        <a href="/#project"><li data-menuanchor="project"></li></a>
        <a href="/#contact"><li data-menuanchor="contact"></li></a>




    </ul>

    <script src="/wp-content/themes/getmaximumwp/onepage/js/app.js?v=3"></script>
    <script src="/wp-content/themes/getmaximumwp/onepage/js/graph-page.js?v=4"></script>