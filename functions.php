<?php
wp_deregister_script('jquery');
add_action( 'after_setup_theme', 'blankslate_setup' );
function blankslate_setup()
{
load_theme_textdomain( 'blankslate', get_template_directory() . '/languages' );
add_theme_support( 'title-tag' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );
global $content_width;
if ( ! isset( $content_width ) ) $content_width = 640;
register_nav_menus(
array( 'main-menu' => __( 'Main Menu', 'blankslate' ) )
);
}
add_action( 'wp_enqueue_scripts', 'blankslate_load_scripts' );
function blankslate_load_scripts()
{
wp_enqueue_script( 'jquery' );
}
add_action( 'comment_form_before', 'blankslate_enqueue_comment_reply_script' );
function blankslate_enqueue_comment_reply_script()
{
if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}
add_filter( 'the_title', 'blankslate_title' );
function blankslate_title( $title ) {
if ( $title == '' ) {
return '&rarr;';
} else {
return $title;
}
}
add_filter( 'wp_title', 'blankslate_filter_wp_title' );
function blankslate_filter_wp_title( $title )
{
return $title . esc_attr( get_bloginfo( 'name' ) );
}
function blankslate_custom_pings( $comment )
{
$GLOBALS['comment'] = $comment;
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php
}
add_filter( 'get_comments_number', 'blankslate_comments_number' );
function blankslate_comments_number( $count )
{
if ( !is_admin() ) {
global $id;
$comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
return count( $comments_by_type['comment'] );
} else {
return $count;
}
}
/*Подключение стилей*/
function true_style_frontend() {
    wp_enqueue_style( 'grid', get_stylesheet_directory_uri() . '/css/grid.css' );
    wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/css/my.css' );
}
add_action( 'wp_enqueue_scripts', 'true_style_frontend' );

/* Отключаем админ панель для всех пользователей. */
  show_admin_bar(false);

  add_theme_support( 'menus' );

/* Сайдбар  */
function arphabet_widgets_init() {

    register_sidebar( array(
        'name'          => 'Правый сайдбар',
        'id'            => 'right',
        'before_widget' => '<aside>',
        'after_widget'  => '</aside>',
        'before_title'  => '<span class="widget-title">',
        'after_title'   => '</span>',
    ) );
    register_sidebar( array(
        'name'          => 'login-box',
        'id'            => 'sloginbox',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ) );

       register_sidebar( array(
        'name'          => 'Подвал 1',
        'id'            => 'footer-1',
        'before_widget' => '<aside class="col-md-2">',
        'after_widget'  => '</aside>',
        'before_title'  => '<span>',
        'after_title'   => '</span>',
    ) );
              register_sidebar( array(
        'name'          => 'Подвал 2',
        'id'            => 'footer-2',
        'before_widget' => '<aside class="col-md-2">',
        'after_widget'  => '</aside>',
        'before_title'  => '<span>',
        'after_title'   => '</span>',
    ) );
                     register_sidebar( array(
        'name'          => 'Подвал 3',
        'id'            => 'footer-3',
        'before_widget' => '<aside class="col-md-2">',
        'after_widget'  => '</aside>',
        'before_title'  => '<span>',
        'after_title'   => '</span>',
    ) );
                            register_sidebar( array(
        'name'          => 'Подвал 4',
        'id'            => 'footer-4',
        'before_widget' => '<aside class="col-md-2">',
        'after_widget'  => '</aside>',
        'before_title'  => '<span>',
        'after_title'   => '</span>',
    ) );
                                   register_sidebar( array(
        'name'          => 'Подвал 5',
        'id'            => 'footer-5',
        'before_widget' => '<aside class="col-md-4">',
        'after_widget'  => '</aside>',
        'before_title'  => '<span>',
        'after_title'   => '</span>',
    ) );

                                    register_sidebar( array(
        'name'          => 'Главная страница',
        'id'            => 'front',
        'before_widget' => '<aside class="col-md-3 col-xs-12">',
        'after_widget'  => '</aside>',
        'before_title'  => '<span class="widget-title">',
        'after_title'   => '</span>',
    ) );


}
add_action( 'widgets_init', 'arphabet_widgets_init' );

