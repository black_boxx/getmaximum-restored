<?php
/**
 * The loop that displays a page
 *
 * The loop displays the posts and the post content. See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop-page.php.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.2
 */
?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>



                    <div id="fullpage" >
				    <!-- <?php post_class(); ?>> -->
					<?php if ( is_front_page() ) { ?>



                            <section data-anchor="main" class="section main">
                                <div class="heading" style="opacity:0.8;">Сети связи будущего</div>
                                <div class="small-heading">проектируем, строим, настраиваем, помогаем поддерживать</div>
                                <div class="bottom">
                                    Инженерная смекалка помноженная на опыт
                                </div>
                                <canvas id="firstcanvas"></canvas>

                            </section>
                            <section data-anchor="master" class="section master">

                                <div class="heading">Мастерски <br>
                                    Душевно <br>
                                    По-человечески!</div>
                                <p>
                                    Мы строим сети основываясь на опыте инженеров и предпочтениях обоих сторон, как заказчиков так и исполнителей. Это значит По-человечески.
                                </p>
                                <p>
                                    Человек, увлеченный любимой работой, делает это с максимальной самоотдачей, ведь такая деятельность приносить удовлетворение душе, это и значит Душевно.
                                </p>
                                <p>
                                    Когда каждый занят по-настоящему "своим" делом - результат получается максимальный! Про таких людей говорят - мастер своего дела.
                                </p>
                                <div id="fittext1" class="fittext1 is-animated ">
                                    Мастерски
                                </div>
                            </section>
                            <section data-anchor="principles" class="section principles">

                                <div class="heading" >Принципы <br>Цели<br> Личности </div>
                                <p class="">
                                    Вам известен принцип “быстро, дешево, качественно, выберите два”.
                                    Мы работаем быстро и качественно, при этом за адекватную цену, которую можно объяснить.
                                    Быстро – значит мы умеем работать эффективно, уважать время - наш конёк.
                                    Качественно – это “как для себя”, с любовью к делу.
                                </p>

                                <p class="">
                                    Наша цель: создавать большие сети по-человечески, основываясь на опыте инженеров и пожеланиях обоих сторон, как заказчиков так и исполнителей. Человек, занятый любимым делом, чувствует себя счастливым и может отдавать максимум. Мы объединяем талантливых инженеров и позволяем им заниматься тем, что им по-душе. Таким образом заказчики и партнёры получают максимум.
                                </p>
                                <p>
                                    Большая часть нашей команды, инженеры с опытом от 10 лет, обладающие отличными навыками в своей области, которые подтверждаются сертификатами и отзывами довольных клиентов.
                                    Мы ценим в людях честность, вне-шаблонное мышление и высокие жизненные ценности. Приоритетом является гармоничное развитие с учетом интересов человека и компании, стремление к синергии.

                                </p>
                                <div id="fittext2"  class="fittext2 is-animated">
                                    Принципы
                                </div>
                                <a href="/quality" class="button">Узнать больше</a>
                            </section>
                            <section data-anchor="profit" class="section profit">
                                <div class="heading">Выгоды</div>
                                <div class="one-third">
                                    <img src="/wp-content/themes/getmaximumwp/onepage/img/vigo1.png" alt="">
                                    <div class="heading2">Ясность цены</div>
                                     <p>
                                        Мы умеем объяснять технически-сложные вещи понятным языком. Вы поймете, за что платите деньги и что получите в итоге. При этом цена будет весьма адекватной.
                                    </p>
                                </div>
                                <div class="one-third">
                                    <img src="/wp-content/themes/getmaximumwp/onepage/img/vigo2.png" alt="">
                                    <div class="heading2">Золотая середина</div>
                                    <p>
                                        Мы придерживаемся принципа <a href="https://ru.wikipedia.org/wiki/KISS_(%D0%BF%D1%80%D0%B8%D0%BD%D1%86%D0%B8%D0%BF)">KISS</a>&nbsp;(Keep&nbsp;it&nbsp;short&nbsp;and&nbsp;simple), который помогает раскрыть потенциал системы, сделать её максимально эффективной и удобной, при этом не усложнив. <br>
                                        Вы получаете Золотую середину, оптимальное соотношение цена/качество, экономите деньги, сохраняете время, система работает как часы.
                                    </p>

                                </div>
                                <div class="one-third">
                                    <img src="/wp-content/themes/getmaximumwp/onepage/img/vigo3.png" alt="">
                                    <div class="heading2">Проверка временем и опытом</div>
                                    <p>
                                        Мы используем проверенные временем и опытом решения, но можем и рискнуть, вместе. Если вы предлагаете свою техническую идею и инженеры её одобряют - мы действуем.<br> Честно предлагаем вам то, с чем реально имеем дело.
                                    </p>
                                </div>

                                <div class="clb"></div>
                                <a class="button popform">GETMAXIMUM</a>
                                <div id="fittext3"  class="fittext3 is-animated">
                                    Выгоды
                                </div>
                            </section>
                            <section data-anchor="services" class="section services vertical-scrolling">
                                <div class="heading" style="margin-top: -70px;">Услуги</div>
                                <div class="small-text">
                                    <b>Cети связи масштаба предприятия.</b> Предлагаем полный спектр услуг: проектируем, строим, настраиваем, помогаем поддерживать, консультируем и проводим аудит.
                                </div>
                               <!--  <div class="slide-nav">
                                    <ul>
                                        <li><a class="nav__item active" href="#service/0">Что делаем</a></li>
                                        <li><a class="nav__item" href="#service/1">Системы</a></li>
                                        <li><a class="nav__item" href="#service/2">Оборудование</a></li>
                                    </ul>
                                </div> -->
                                <div id="fittext4"  style="position:absolute;bottom:50px;" class="fittext4">
                                    Услуги
                                </div>

                                <canvas class="graph" id="viewport" ></canvas>
                       <!--          <div class="slides">
                                    <div class="horizontal-scrolling">
                                        <img class="mesh is-animated" src="/wp-content/themes/getmaximumwp/onepage/img/mesh.jpg" alt="">
                                        <div class="desc">
                                            <b>Что делаем</b><br>
                                            Проектируем сети, строим, настраиваем, помогаем поддерживать, консультируем, проводим профессиональный аудит технических систем.
                                        </div>
                                    </div>
                                    <div class="horizontal-scrolling">
                                        <img class="wifi" src="/wp-content/themes/getmaximumwp/onepage/img/router.png" alt="">
                                        <div class="desc">
                                            <b>Какие системы</b><br>
                                            Проводные и беспроводные сети передачи данных, серверная инфраструктура, видеонаблюдение и информационная безопасность. Комплексная интеграция систем.
                                        </div>
                                    </div>
                                    <div class="horizontal-scrolling obor">
                                        <img src="/wp-content/themes/getmaximumwp/onepage/img/vendors.jpg" alt="">
                                        <div class="obo-desc desc">
                                        <b>Оборудование</b><br>

                                        Cisco, Mikrotik, Huawei, Juniper, Extreme, Ubiquiti, HP, Fujitsu, Dell и т.д.<br>
                                        <p><i>
                                            Существует <a href="http://www.rugost.com/index.php?option=com_content&view=article&id=95:gost-34-601-90-avtomatizirovannye-sistemy-stadii-sozdaniya&catid=22&Itemid=53">ГОСТ 34.601-90 </a>который устанавливает стадии и этапы создания автоматизированных систем. Мы придерживаемся этого стандарта и своими силами выполняем разработку, пуско-наладку, сопровождение и аудит. Наши проверенные партнёры занимаются поставкой и монтажем оборудования.
                                            </i>
                                            </p>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="bottom">
                                    <div class="clb"></div>
                                    <a href="#GETMAXIMUM" class="button popform">GETMAXIMUM</a>
                                    <div class="text">
                                        Что нужно вам, станет ясно в процессе написания технического задания, без которого мы, увы, не сможем строить с вами деловые отношения. Если у вас есть готовое ТЗ, или его черновой вариант – пишите нам.
                                    </div>
                                </div>
                            </section>
                            <section data-anchor="project" class="section project ">
                                <div class="heading">Проекты</div>
                                <div class="dopinfo">
                                    В каждом проекте завязываются добрые отношения, если все довольны работой.<br> Технические консультации по понятным вопросам мы даём бесплатно, неограниченное время после запуска системы.<br> Поэтому, по каждому проекту, человек, кто его принимал, может дать хороший отзыв. Мы это ценим.
                                </div>
                                <div class="logos">
                                    <img title="Узнайте подробнее" src="/wp-content/themes/getmaximumwp/onepage/img/proj/3.png" alt="">
                                    <img title="Узнайте подробнее" src="/wp-content/themes/getmaximumwp/onepage/img/proj/1.png" alt="">

                                    <img title="Узнайте подробнее" src="/wp-content/themes/getmaximumwp/onepage/img/proj/4.png" alt="">
                                    <img title="Узнайте подробнее" src="/wp-content/themes/getmaximumwp/onepage/img/proj/5.png" alt="">
                                    <img title="Узнайте подробнее" src="/wp-content/themes/getmaximumwp/onepage/img/proj/6.png" alt="">
                                    <img title="Узнайте подробнее" src="/wp-content/themes/getmaximumwp/onepage/img/proj/7.png" alt="">
                                    <img title="Узнайте подробнее" src="/wp-content/themes/getmaximumwp/onepage/img/proj/8.png" ajpg="">
                                </div>

                                <div class="bottom">
                                    <div class="clb"></div>
                                    <a href="/projects" class="button">Подробнее</a>
                                </div>

                                <div id="fittext5"  class="fittext5">
                                    Проекты
                                </div>
                            </section>
                            <section data-anchor="contact" class="section contact">
                                <div class="heading">Контакты</div>
                                <img class="mail" src="/wp-content/themes/getmaximumwp/onepage/img/mail.png" alt="">
                                <div class="desc">
                                    г.Екатеринбург ул. Бориса Ельцина 3а, 3й этаж <br>
                                    +7(343)361 71 08<br>
                                    <a href="mailto:info@getmaximum.info">info@getmaximum.info</a><br>
                                </div>
                                <div class="clb"></div>
                                <div class="button popform">Свяжитесь с нами</div>
                                <div id="fittext6"  class="fittext6">
                                    Ом Тат Сат
                                </div>
                            </section>



                        </div>





					<?php } else { ?>
						<h2 class="entry-title"><?php the_title(); ?></h2>
                        <div class="entry-content">
                            <?php the_content(); ?>
                            <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
                            <?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
                        </div><!-- .entry-content -->
					<?php } ?>



				<?php comments_template( '', true ); ?>

<?php endwhile; // end of the loop. ?>
