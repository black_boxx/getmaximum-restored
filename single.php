<?php
/**
 * Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container" <?php if(in_category(71)) {
                    echo "class='etap'";}?> >

            <?php
                if(in_category(71)) {
                    echo "<div class='left-side-bar'>";
                    if ( is_active_sidebar( 'left-sidebar' )) {
                        dynamic_sidebar( 'left-sidebar' );
                    }
                    echo "</div>";
                }


            ?>

            <script>

            </script>

			<div id="content" role="main">

			<?php
			/*
			 * Run the loop to output the post.
			 * If you want to overload this in a child theme then include a file
			 * called loop-single.php and that will be used instead.
			 */
			get_template_part( 'loop', 'single' );
			?>

			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
